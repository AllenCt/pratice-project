package app;

import app.demo.CustomerAJAXWebService;
import app.demo.api.CustomerWebService;
import app.demo.customer.web.CustomerAJAXWebServiceImpl;
import core.framework.module.Module;

/**
 * @author Allen
 */
public class CustomerModule extends Module {
    @Override
    protected void initialize() {
        api().client(CustomerWebService.class, requiredProperty("app.CustomerWebServiceUrl"));

        api().service(CustomerAJAXWebService.class, bind(CustomerAJAXWebServiceImpl.class));
    }
}
