package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author Allen
 */
public class DemoSiteApp extends App {
    @Override
    protected void initialize() {
        http().httpPort(8082);
        load(new SystemModule("app.properties"));

        load(new CustomerModule());
        load(new OrderModule());
    }
}
