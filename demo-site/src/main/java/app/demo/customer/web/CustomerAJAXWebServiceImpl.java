package app.demo.customer.web;

import app.demo.CustomerAJAXWebService;
import app.demo.api.CustomerWebService;
import app.demo.api.customer.CustomerView;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    CustomerWebService customerWebService;

    @Override
    public CustomerView get(Long id) {
        return customerWebService.get(id);
    }

}
