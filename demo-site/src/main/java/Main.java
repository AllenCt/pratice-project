import app.DemoSiteApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoSiteApp().start();
    }
}
