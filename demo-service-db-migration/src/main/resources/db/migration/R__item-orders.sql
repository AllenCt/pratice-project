CREATE TABLE IF NOT EXISTS `orders` (
    `id` VARCHAR(30) NOT NULL,
    `order_name` VARCHAR(30) NOT NULL,
    `price`  DOUBLE ,
    `address` VARCHAR(50) NOT NULL,
    `customer_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;