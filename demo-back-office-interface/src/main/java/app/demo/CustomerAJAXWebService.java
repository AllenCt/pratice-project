package app.demo;

import app.demo.api.customer.SearchCustomerRequest;
import app.demo.api.customer.SearchCustomerResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface CustomerAJAXWebService {
    @GET
    @Path("/customer")
    SearchCustomerResponse search(SearchCustomerRequest request);
}
