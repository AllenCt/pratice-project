package app.demo.job;

import core.framework.scheduler.Job;
import core.framework.scheduler.JobContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;

/**
 * @author Allen
 */
public class DemoJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(DemoJob.class);

    @Override
    public void execute(JobContext context) throws InterruptedException {
        logger.debug("run job, name={}, time={}", context.name, context.scheduledTime);
        logger.info("------------job start-------------");
        logger.info(String.valueOf(ZonedDateTime.now()));
        logger.info(Thread.currentThread().getName());
        Thread.sleep(3000);
        logger.info("------------job finish------------");
    }
}
