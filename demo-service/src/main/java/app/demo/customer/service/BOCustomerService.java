package app.demo.customer.service;

import app.demo.api.customer.CustomerView;
import app.demo.api.customer.SearchCustomerRequest;
import app.demo.api.customer.SearchCustomerResponse;
import app.demo.customer.domain.Bloginfo;
import app.demo.customer.domain.Customer;
import core.framework.db.Database;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import core.framework.util.Strings;

import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class BOCustomerService {
    @Inject
    private Repository<Customer> customerRepository;
    @Inject
    MongoCollection<Bloginfo> bloginfoMongoCollection;
    @Inject
    Database database;

    public SearchCustomerResponse search(SearchCustomerRequest request) {
        SearchCustomerResponse searchCustomerResponse = new SearchCustomerResponse();
        Query<Customer> query = customerRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.customerName)) {
            query.where("customer_name like ?", Strings.format("{}%", request.customerName));
        }
        if (!Strings.isBlank(request.phone)) {
            query.where("phone = ?", request.phone);
        }
        searchCustomerResponse.customers = query.fetch().stream().map(this::view).collect(Collectors.toList());
        searchCustomerResponse.total = Long.valueOf(query.count());
        return searchCustomerResponse;
    }

    private CustomerView view(Customer customer) {
        CustomerView customerView = new CustomerView();
        customerView.id = customer.id;
        customerView.customerName = customer.customerName;
        customerView.phone = customer.phone;
        return customerView;
    }
}
