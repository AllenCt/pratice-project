package app.demo.customer.web;

import app.demo.api.BOCustomerWebService;
import app.demo.api.customer.SearchCustomerRequest;
import app.demo.api.customer.SearchCustomerResponse;
import app.demo.customer.service.BOCustomerService;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class BOCustomerWebServiceImpl implements BOCustomerWebService {
    @Inject
    private BOCustomerService boCustomerService;

    @Override
    public SearchCustomerResponse search(SearchCustomerRequest request) {
        return boCustomerService.search(request);
    }
}
