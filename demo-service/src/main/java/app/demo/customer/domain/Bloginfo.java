package app.demo.customer.domain;

import core.framework.mongo.Collection;
import core.framework.mongo.Field;
import core.framework.mongo.Id;
import org.bson.types.ObjectId;

/**
 * @author Allen
 */
@Collection(name = "bloginfos")
public class Bloginfo {
    @Id
    public ObjectId id;

    @Field(name = "name")
    public String name;

    @Field(name = "blog_info")
    public String blogInfo;
}
