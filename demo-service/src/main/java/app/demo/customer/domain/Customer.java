package app.demo.customer.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

/**
 * @author Allen
 */
@Table(name = "customers")
public class Customer {

    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Long id;

    @NotNull
    @Column(name = "customer_name")
    public String customerName;

    @Column(name = "phone")
    public String phone;
}
