package app.demo.order.service;

import app.demo.api.order.SearchOrderAndCustomerView;
import app.demo.api.order.SearchOrderRequest;
import app.demo.api.order.SearchOrderResponse;
import app.demo.customer.domain.Customer;
import app.demo.order.domain.Order;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.util.stream.Collectors;

/**
 * @author Allen
 */
public class OrderService {
    @Inject
    Repository<Order> orderRepository;
    @Inject
    Repository<Customer> customerRepository;

    public SearchOrderResponse search(SearchOrderRequest request) {
        SearchOrderResponse response = new SearchOrderResponse();
        Query<Order> query = orderRepository.select();
        query.skip(request.skip);
        query.limit(request.limit);
        if (!Strings.isBlank(request.id)) {
            query.where("id = ?", request.id);
        }
        if (!Strings.isBlank(request.orderName)) {
            query.where("order_name like ?", Strings.format("{}%", request.orderName));
        }
        if (!Strings.isBlank(request.address)) {
            query.where("address like ?", Strings.format("{}%", request.address));
        }
        if (request.customerId != null) {
            query.where("customer_id = ?", request.customerId);
        }
        response.orderAndCustomerViews = query.fetch().stream().map(this::view).collect(Collectors.toList());
        response.total = Long.valueOf(query.count());
        return response;
    }

    private SearchOrderAndCustomerView view(Order order){
        SearchOrderAndCustomerView searchOrderAndCustomerView = new SearchOrderAndCustomerView();
        Customer customer = customerRepository.get(order.customerId).orElseThrow(() -> new NotFoundException("customer not found id" + order.customerId));
        searchOrderAndCustomerView.id = Long.valueOf(order.id);
        searchOrderAndCustomerView.address = order.address;
        searchOrderAndCustomerView.customerName = customer.customerName;
        searchOrderAndCustomerView.phone = customer.phone;
        return searchOrderAndCustomerView;
    }
}
