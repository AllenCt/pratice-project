package app.demo;

import app.DemoServiceApp;
import core.framework.test.module.AbstractTestModule;

/**
 * @author Allen
 */
public class TestModule extends AbstractTestModule {
    @Override
    protected void initialize() {
        load(new DemoServiceApp());
    }
}
