package app.demo;

import app.demo.api.customer.CustomerView;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author Allen
 */
public interface CustomerAJAXWebService {
    @GET
    @Path("/customer/:id")
    CustomerView get(@PathParam("id") Long id);
}
