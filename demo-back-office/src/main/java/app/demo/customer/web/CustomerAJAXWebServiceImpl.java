package app.demo.customer.web;

import app.demo.CustomerAJAXWebService;
import app.demo.api.BOCustomerWebService;
import app.demo.api.customer.SearchCustomerRequest;
import app.demo.api.customer.SearchCustomerResponse;
import core.framework.inject.Inject;

/**
 * @author Allen
 */
public class CustomerAJAXWebServiceImpl implements CustomerAJAXWebService {
    @Inject
    private BOCustomerWebService boCustomerWebService;

    @Override
    public SearchCustomerResponse search(SearchCustomerRequest request) {

        return boCustomerWebService.search(request);
    }
}
