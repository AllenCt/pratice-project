package app.demo.api;

import app.demo.api.customer.SearchCustomerRequest;
import app.demo.api.customer.SearchCustomerResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author Allen
 */
public interface BOCustomerWebService {
    @GET
    @Path("/bo/customer")
    SearchCustomerResponse search(SearchCustomerRequest request);
}
