package app.demo.api.customer.bloginfo;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class CreateBlogInfoRequest {
    @Property(name = "name")
    public String name;

    @Property(name = "blog_info")
    public String blogInfo;
}
