package app.demo.api.customer.bloginfo;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author Allen
 */
public class SearchBlogInfoResponse {
    @Property(name = "total")
    public Integer total;

    @Property(name = "blog_info")
    public List<BlogInfoView> blogInfoViews;
}
