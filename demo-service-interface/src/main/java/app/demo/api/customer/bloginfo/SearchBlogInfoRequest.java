package app.demo.api.customer.bloginfo;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author Allen
 */
public class SearchBlogInfoRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip = 0;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit = 1000;

    @QueryParam(name = "name")
    public String name;

    @QueryParam(name = "blog_info")
    public String blogInfo;
}
