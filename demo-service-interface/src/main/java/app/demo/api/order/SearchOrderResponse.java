package app.demo.api.order;

import core.framework.api.json.Property;

import java.util.List;

/**
 * @author Allen
 */
public class SearchOrderResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "order_and_customer")
    public List<SearchOrderAndCustomerView> orderAndCustomerViews;
}
