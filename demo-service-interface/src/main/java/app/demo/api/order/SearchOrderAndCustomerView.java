package app.demo.api.order;

import core.framework.api.json.Property;

/**
 * @author Allen
 */
public class SearchOrderAndCustomerView {
    @Property(name = "id")
    public Long id;

    @Property(name = "address")
    public String address;

    @Property(name = "customer_name")
    public String customerName;

    @Property(name = "phone")
    public String phone;
}
