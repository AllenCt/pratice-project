package app.demo.api.order;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author Allen
 */
public class OrderView {
    @Property(name = "id")
    public Long id;

    @NotNull
    @Property(name = "order_name")
    public String orderName;

    @Property(name = "price")
    public String price;

    @Property(name = "address")
    public String address;

    @Property(name = "customer_id")
    public String customerId;
}
