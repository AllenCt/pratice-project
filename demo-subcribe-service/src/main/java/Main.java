import app.DemoSubcribeApp;

/**
 * @author Allen
 */
public class Main {
    public static void main(String[] args) {
        new DemoSubcribeApp().start();
    }
}
